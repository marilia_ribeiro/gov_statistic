from import_info.requests import (CreateImportsRequest,
                                   ExportImportsToCsvRequest,
                                   GetImportsRequest)
from setup import setup


def main():
    imports_uc = setup()

    create_req = CreateImportsRequest(file_path='CAPI011903.CSV', chapter='1', month_year='03-2019')
    get_req = GetImportsRequest(chapter='1', month_year='03-2019')
    export_req = ExportImportsToCsvRequest(chapter='1', month_year='03-2019')


    print('----- Create Imports -----')
    resp = imports_uc.create_imports(request=create_req)
    print(resp)

    #print('----- Get Imports -----')
    #resp = imports_uc.get_imports(request=get_req)
    #print(resp)

    #print('----- Export Imports -----')
    #resp = imports_uc.export_imports_to_csv(request=export_req)
    #print(resp)


if __name__ == "__main__":
    main()
