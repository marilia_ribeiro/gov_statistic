import pandas as pd

from .consts import COLUMN_NAMES
from .entities import Country, ImportInfo, Ncm, Statistic

FILE_PATH = 'CAPI011903.CSV'

def read_file(file_path=FILE_PATH, chapter=1, month_year='03-2019'):
	df = pd.read_csv(file_path,  delimiter='@', encoding='ISO-8859-1', names=COLUMN_NAMES, header=None, skiprows=1)
	
	imports_info = []
	for index, row in df.iterrows():
		import_info = ImportInfo(
			order_number=int(row['order_number']),
			chapter=chapter,
			month_year=month_year,
			ncm=Ncm(
				id=int(row['ncm_id']),
				description=row['ncm_description'].strip()
			),
			origin_country=Country(
				id=int(row['origin_country_id']),
				name=row['origin_country'].strip()
			),
			acquisition_country=Country(
				id=int(row['acquisition_country_id']),
				name=row['acquisition_country'].strip()
			),
			statistic=Statistic(
				id=int(row['statistic_id']),
				measurement_unit=row['statistic_measurement_unit'].strip()
			),
			commercial_unit=row['commercial_unit'].strip(),
			product_description=row['product_description'].strip(),
			statistic_qtd=float(row['statistic_qtd'].strip().replace(',','')),
			net_weight=float(row['net_weight'].strip().replace(',','')),
			vmle_dolar=float(row['vmle_dolar'].strip().replace(',','')),
			vl_dollar_shipping=float(row['vl_dollar_shipping'].strip().replace(',','')),
			vl_dollar_insurance=float(row['vl_dollar_insurance'].strip().replace(',','')),
			vl_dolar_product_unity=float(row['vl_dolar_product_unity'].strip().replace(',','')),
			commercial_qtd=float(row['commercial_qtd'].strip().replace(',','')),
			dolar_product_unity_total=float(row['dolar_product_unity_total'].strip().replace(',','')),
			landing_unit=row['landing_unit'].strip(),
			clearance_unit=row['clearance_unit'].strip(),
			incoterm=row['incoterm'].strip(),
			national_information=row['national_information'].strip(),
			dispatch_situation=row['dispatch_situation'].strip()
		)
		imports_info.append(import_info)
	
	return imports_info
