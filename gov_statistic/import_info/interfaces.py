import abc
from typing import List

from .entities import ImportInfo
from .requests import (CreateImportsRequest, ExportImportsToCsvRequest,
                       GetImportsRequest)


class IImportsRepo(abc.ABCMeta):
	@abc.abstractmethod
	def insert_imports(self, imports_info: List[ImportInfo]):
		raise NotImplementedError
	
	@abc.abstractmethod
	def get_imports(self, chapter: int, month_year: str):
		raise NotImplementedError

class IImportsUseCases(abc.ABCMeta):
	@abc.abstractmethod
	def create_imports(self, request: CreateImportsRequest):
		raise NotImplementedError
	
	@abc.abstractmethod
	def get_imports(self, request: GetImportsRequest):
		raise NotImplementedError
	
	@abc.abstractmethod
	def export_imports_to_csv(self, request: ExportImportsToCsvRequest):
		raise NotImplementedError
