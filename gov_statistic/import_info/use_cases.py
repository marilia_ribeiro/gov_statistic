from common import Resp, to_csv
from .consts import COLUMN_NAMES
from .entities import ImportInfo
from .interfaces import IImportsRepo, IImportsUseCases
from .preprocess import read_file
from .requests import (CreateImportsRequest, ExportImportsToCsvRequest,
                       GetImportsRequest)

FILE_PATH = '~/Downloads/imports_{chapter}_{month_year}.csv'


class ImportsUseCases(IImportsUseCases):
	def __init__(self, repo: IImportsRepo):
		self.repo = repo
		
	def create_imports(self, request: CreateImportsRequest):
		imports_info = read_file(
			file_path=request.file_path,
			chapter=request.chapter,
			month_year=request.month_year
		)
		return self.repo.insert_imports(imports_info=imports_info)

		
	def get_imports(self, request: GetImportsRequest):
		return self.repo.get_imports(
			chapter=request.chapter,
			month_year=request.month_year
		)
	
	def export_imports_to_csv(self, request: ExportImportsToCsvRequest):
		imports_info = self.repo.get_imports(
			chapter=request.chapter,
			month_year=request.month_year
		)
		data = [item.to_dict() for item in imports_info]

		file_path = FILE_PATH.format(
			chapter=request.chapter,
			month_year=request.month_year
		)
		to_csv(file_path=file_path, data=data, columns=COLUMN_NAMES)
		return Resp(payload='File exported to {}'.format(file_path))
