from typing import List

from .entities import ImportInfo
from .interfaces import IImportsRepo
from .consts import COLUMN_NAMES

class ImportsRepo(IImportsRepo):
	def __init__(self, connection):
		self.connection = connection

	def insert_imports(self, imports_info: List[ImportInfo]):
		sql = sql_insert(imports_info)
		return self.connection.insert(sql)
		
	def get_imports(self, chapter: int, month_year: str):
		sql = "select * from public.import_info where chapter={chapter} and month_year='{month_year}';".format(
			chapter=chapter,
			month_year=month_year
		)
		db_imports_info = self.connection.select(sql)
		return to_imports_info(db_imports_info)

def sql_insert(imports_info: List[ImportInfo]):
	columns = ', '.join(COLUMN_NAMES)
	values = ['{}'.format(tuple(item.to_dict().values())) for item in imports_info]	
	values = ', '.join(values)

	return "insert into public.import_info ({columns}) values {values};".format(
		columns=columns,
		values=values
	)


def to_imports_info(db_imports_info):
	imports_info = []
	return imports_info
