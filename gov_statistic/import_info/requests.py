from dataclasses import dataclass

@dataclass
class CreateImportsRequest:
	file_path: str = None
	chapter: int = None
	month_year: str = None

@dataclass
class GetImportsRequest:
	chapter: int = None
	month_year: str = None

@dataclass
class ExportImportsToCsvRequest:
	chapter: int = None
	month_year: str = None
