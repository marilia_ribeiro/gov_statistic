from dataclasses import dataclass

@dataclass
class Statistic:
    id: int = None
    measurement_unit: str = None

@dataclass
class Country:
    id: int = None
    name: str = None

@dataclass
class Ncm:
    id: int = None
    description: str = None

@dataclass
class ImportInfo:
    order_number: int = None
    chapter: int = None
    month_year: str = None
    ncm: Ncm = None
    origin_country: Country = None
    acquisition_country: Country = None
    statistic: Statistic = None
    commercial_unit: str = None
    product_description: str = None 
    statistic_qtd: float = None
    net_weight: float = None
    vmle_dolar: float = None
    vl_dollar_shipping: float = None
    vl_dollar_insurance: float = None
    vl_dolar_product_unity: float = None
    commercial_qtd: float = None
    dolar_product_unity_total: float = None
    landing_unit: str = None
    clearance_unit: str = None
    incoterm: str = None
    national_information: str = None
    dispatch_situation: str = None

    def to_dict(self):
        return {
            'order_number': self.order_number,
            'chapter': self.chapter,
            'month_year': self.month_year,
            'ncm_id': self.ncm.id,
            'ncm_description': self.ncm.description,
            'origin_country_id': self.origin_country.id,
            'origin_country': self.origin_country.name,
            'acquisition_country_id': self.acquisition_country.id,
            'acquisition_country': self.acquisition_country.name,
            'statistic_id': self.statistic.id,
            'statistic_measurement_unit': self.statistic.measurement_unit,
            'commercial_unit': self.commercial_unit,
            'product_description': self.product_description,
            'statistic_qtd': self.statistic_qtd,
            'net_weight': self.net_weight,
            'vmle_dolar': self.vmle_dolar,
            'vl_dollar_shipping': self.vl_dollar_shipping,
            'vl_dollar_insurance': self.vl_dollar_insurance,
            'vl_dolar_product_unity': self.vl_dolar_product_unity,
            'commercial_qtd': self.commercial_qtd,
            'dolar_product_unity_total': self.dolar_product_unity_total,
            'landing_unit': self.landing_unit,
            'clearance_unit': self.clearance_unit,
            'incoterm': self.incoterm,
            'national_information': self.national_information,
            'dispatch_situation': self.dispatch_situation,
        }