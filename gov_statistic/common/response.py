from dataclasses import dataclass
from typing import Any


@dataclass
class Resp():
    status: bool = True
    error: str = None
    payload: Any = None

    @property
    def ok(self):
        return True if self.status else False
