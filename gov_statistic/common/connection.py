import psycopg2

HOST = 'localhost'
DATABASE = 'gov_import_export'
USER = 'postgres'
PASSWORD='postgres'

class Connection():
	def __init__(self, host=HOST, database=DATABASE, user=USER, password=PASSWORD):
		self.db = psycopg2.connect(host=host, database=database, user=user,  password=password)
		self.cursor = self.db.cursor()
		
	def insert(self, sql):
		try:
			self.cursor.execute(sql)
			self.cursor.close()
			self.db.commit()
		except:
			return False
		return True
	 
	def select(self, sql):
		resp = None
		try:
			self.cursor.execute(sql)
			resp = self.cursor.fetchall()
		except:
			return None
		return resp

	def close(self):
		self.db.close()
