from typing import List

import pandas as pd


def to_csv(file_path: str, data: dict, columns: List[str]):
    df = pd.DataFrame(data, columns=columns)
    df.to_csv(file_path, sep=';', index=None, header=True, encoding='utf-8')
