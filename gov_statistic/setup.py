from common.connection import Connection
from import_info.repo import ImportsRepo
from import_info.use_cases import ImportsUseCases


def setup():
    connection = Connection()

    imports_repo = ImportsRepo(connection=connection)
    imports_uc = ImportsUseCases(repo=imports_repo)

    return imports_uc
