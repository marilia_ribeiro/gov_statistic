create database gov_import_export;

create table import_info (
    order_number integer primary key,
    chapter integer,
    month_year varchar(7),
    ncm_id integer,
    ncm_description varchar(100),
    origin_country_id integer,
    origin_country varchar(50),
    acquisition_country_id integer,
    acquisition_country varchar(50),
    statistic_id integer,
    statistic_measurement_unit varchar(50),
    commercial_unit varchar(50),
    product_description text,
    statistic_qtd float,
    net_weight float,
    vmle_dolar float,
    vl_dollar_shipping float,
    vl_dollar_insurance float,
    vl_dolar_product_unity float,
    commercial_qtd float,
    dolar_product_unity_total float,
    landing_unit varchar(100),
    clearance_unit varchar(100),
    incoterm varchar(100),
    national_information varchar(100),
    dispatch_situation varchar(100)
);
create index  import_info_order_number_idx ON import_info (order_number);
create index  import_info_chapter_idx ON import_info (chapter);
create index  import_info_month_year_idx ON import_info (month_year);