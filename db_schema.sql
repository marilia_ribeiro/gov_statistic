--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: import_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.import_info (
    order_number integer NOT NULL,
    chapter integer,
    month_year character varying(7),
    ncm_id integer,
    ncm_description character varying(100),
    origin_country_id integer,
    origin_country character varying(50),
    acquisition_country_id integer,
    acquisition_country character varying(50),
    statistic_id integer,
    statistic_measurement_unit character varying(50),
    commercial_unit character varying(50),
    product_description text,
    statistic_qtd double precision,
    net_weight double precision,
    vmle_dolar double precision,
    vl_dollar_shipping double precision,
    vl_dollar_insurance double precision,
    vl_dolar_product_unity double precision,
    commercial_qtd double precision,
    dolar_product_unity_total double precision,
    landing_unit character varying(100),
    clearance_unit character varying(100),
    incoterm character varying(100),
    national_information character varying(100),
    dispatch_situation character varying(100)
);


ALTER TABLE public.import_info OWNER TO postgres;

--
-- Name: import_info import_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.import_info
    ADD CONSTRAINT import_info_pkey PRIMARY KEY (order_number);


--
-- Name: import_info_chapter_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX import_info_chapter_idx ON public.import_info USING btree (chapter);


--
-- Name: import_info_month_year_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX import_info_month_year_idx ON public.import_info USING btree (month_year);


--
-- Name: import_info_order_number_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX import_info_order_number_idx ON public.import_info USING btree (order_number);


--
-- PostgreSQL database dump complete
--

